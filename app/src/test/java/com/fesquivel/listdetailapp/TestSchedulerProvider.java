package com.fesquivel.listdetailapp;

import com.fesquivel.listdetailapp.commons.BaseSchedulerProvider;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class TestSchedulerProvider implements BaseSchedulerProvider {

    private final Scheduler ioScheduler;
    private final Scheduler computationScheduler;
    private final Scheduler uiScheduler;

    /**
     * Constructor
     *
     * @param ioScheduler The I/O Scheduler
     * @param computationScheduler The Computation Scheduler
     * @param uiScheduler The UI Scheduler
     */
    public TestSchedulerProvider(final Scheduler ioScheduler, final Scheduler computationScheduler,
                                    final Scheduler uiScheduler) {
        this.ioScheduler = ioScheduler;
        this.computationScheduler = computationScheduler;
        this.uiScheduler = uiScheduler;
    }

    public static BaseSchedulerProvider defaultProvider() {
        return new TestSchedulerProvider(Schedulers.trampoline(), Schedulers.trampoline(), Schedulers.trampoline());
    }

    @Override
    public Scheduler io() {
        return ioScheduler;
    }

    @Override
    public Scheduler computation() {
        return computationScheduler;
    }

    @Override
    public Scheduler ui() {
        return uiScheduler;
    }
}