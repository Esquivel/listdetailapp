package com.fesquivel.listdetailapp.home.repository.user;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DataUserRepositoryTest {

    private static final String USER_KEY = "user_key";
    private static final String STRING = "STRING";

    private UserRepository userRepository;
    private SharedPreferences sharedPreferences;
    private Gson gson;

    @Before
    public void setUp() {
        sharedPreferences = mock(SharedPreferences.class);
        gson = mock(Gson.class);

        userRepository = new DataUserRepository(sharedPreferences, gson);
    }

    @Test
    public void whenGetCalledThenShouldUseKeyDefinedToUserData() {
        userRepository.get();

        verify(sharedPreferences).getString(USER_KEY, null);
    }

    @Test
    public void whenSaveCalledThenShouldUseKeyDefinedToUserData() {
        final User user = mock(User.class);
        final SharedPreferences.Editor editor = mock(SharedPreferences.Editor.class);
        when(sharedPreferences.edit()).thenReturn(editor);
        when(gson.toJson(user)).thenReturn(STRING);

        userRepository.save(user);

        verify(editor).putString(USER_KEY, STRING);
    }
    
    @Test
    public void whenGetAndHasDataThenReturnDataStored() {
        final DataUserRepository.InternalUser user = mock(DataUserRepository.InternalUser.class);
        when(sharedPreferences.getString(USER_KEY, null)).thenReturn(STRING);
        when(gson.fromJson(STRING, DataUserRepository.InternalUser.class)).thenReturn(user);

        Assert.assertEquals(user, userRepository.get());
    }

    @Test
    public void whenSaveUserThenShouldCallToApplyMethod() {
        final User user = mock(User.class);
        final SharedPreferences.Editor editor = mock(SharedPreferences.Editor.class);
        when(sharedPreferences.edit()).thenReturn(editor);

        userRepository.save(user);

        verify(editor).apply();
    }
}