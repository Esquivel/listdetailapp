package com.fesquivel.listdetailapp.home.ui;

import com.fesquivel.listdetailapp.TestSchedulerProvider;
import com.fesquivel.listdetailapp.commons.ButtonType;
import com.fesquivel.listdetailapp.commons.SchedulerProvider;
import com.fesquivel.listdetailapp.home.domain.Banner;
import com.fesquivel.listdetailapp.home.domain.Button;
import com.fesquivel.listdetailapp.home.domain.FeedSale;
import com.fesquivel.listdetailapp.home.domain.Item;
import com.fesquivel.listdetailapp.home.domain.Sale;
import com.fesquivel.listdetailapp.home.repository.sale.SaleRepository;
import com.fesquivel.listdetailapp.home.repository.user.User;
import com.fesquivel.listdetailapp.home.repository.user.UserRepository;

import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class HomePresenterTest {

    private static final String STRING = "STRING";

    private HomePresenter presenter;
    private SaleRepository saleRepository;
    private HomeView<HomePresenter> view;
    private HomeState state;

    @Before
    public void setUp() {
        final UserRepository userRepository = mock(UserRepository.class);
        final CompositeDisposable compositeDisposable = mock(CompositeDisposable.class);
        saleRepository = mock(SaleRepository.class);
        view = (HomeView<HomePresenter>) mock(HomeView.class);
        state = mock(HomeState.class);

        presenter = new HomePresenter(saleRepository, userRepository,
                TestSchedulerProvider.defaultProvider(), compositeDisposable);
        presenter.setView(view);
    }

    @Test
    public void whenHasInvalidUserDataThenShowLogin() {
        presenter.setState(state);
        when(state.getUser()).thenReturn(null);

        presenter.onRefresh();

        verify(view).showLoginDialog();
    }

    @Test
    public void whenClickCardThenStartDetailScreen() {
        final Sale sale = mock(Sale.class);
        final Item item = mock(Item.class);
        final Banner banner = mock(Banner.class);
        final User user = mock(User.class);
        when(state.getUser()).thenReturn(user);
        when(user.getUserId()).thenReturn(STRING);
        when(sale.getBanner()).thenReturn(banner);
        when(sale.getItem()).thenReturn(item);
        presenter.setState(state);

        presenter.onCardClicked(sale);

        verify(view).startDetailScreen(STRING, item, banner);
    }

    @Test
    public void whenClickButtonDisabledThenShouldNotSaveItem() {
        final Sale sale = mock(Sale.class);
        final Button button = mock(Button.class);
        when(sale.getButton()).thenReturn(button);
        when(button.getType()).thenReturn(ButtonType.DISABLED);

        presenter.onItemButtonClicked(sale);

        verify(saleRepository, never()).saveItem(any(), any());
    }

    @Test
    public void whenClickButtonLockedThenShouldNotSaveItem() {
        final Sale sale = mock(Sale.class);
        final Button button = mock(Button.class);
        when(sale.getButton()).thenReturn(button);
        when(button.getType()).thenReturn(ButtonType.LOCKED);

        presenter.onItemButtonClicked(sale);

        verify(saleRepository, never()).saveItem(any(), any());
    }
}
