package com.fesquivel.listdetailapp.detail.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DetailMapperTest {

    private DetailMapper mapper;

    @Before
    public void setUp() {
        mapper = new DetailMapper();
    }

    @Test
    public void whenMapEmptyListReturnsEmptyList() {
        Assert.assertEquals(Collections.emptyList(), mapper.mapTo(Collections.emptyList()));
    }

    @Test
    public void whenMapNullReturnsEmptyList() {
        Assert.assertEquals(Collections.emptyList(), mapper.mapTo(null));
    }

    @Test
    public void whenMapOneValidValueReturnsOneValuesList() {
        final DetailDto validDto = DetailDto.createExpiration(1);
        final List<DetailDto> detailDtoList = new ArrayList<>();
        detailDtoList.add(validDto);

        Assert.assertEquals(1, mapper.mapTo(detailDtoList).size());
    }

    @Test
    public void whenMapOneInvalidValueReturnsEmptyList() {
        final DetailDto validDto = new DetailDto(12312, null, null,
                null, null, null , null);
        final List<DetailDto> detailDtoList = new ArrayList<>();
        detailDtoList.add(validDto);

        Assert.assertEquals(Collections.emptyList(), mapper.mapTo(detailDtoList));
    }
}