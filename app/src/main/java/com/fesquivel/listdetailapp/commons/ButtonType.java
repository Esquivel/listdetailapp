package com.fesquivel.listdetailapp.commons;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@IntDef({
        ButtonType.LOCKED,
        ButtonType.ENABLED,
        ButtonType.DISABLED
})
public @interface ButtonType {
    int LOCKED = 0;
    int ENABLED = 1;
    int DISABLED = 2;
}