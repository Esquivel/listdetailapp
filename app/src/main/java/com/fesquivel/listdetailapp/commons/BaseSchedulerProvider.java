package com.fesquivel.listdetailapp.commons;

import io.reactivex.Scheduler;

public interface BaseSchedulerProvider {

    /**
     * Returns an instance of Schedulers.io()
     * @return A {@link rx.schedulers.Schedulers}.
     */
    Scheduler io();

    /**
     * Returns an instance of Schedulers.computation()
     * @return A {@link rx.schedulers.Schedulers}.
     */
    Scheduler computation();

    /**
     * Returns an instance of AndroidSchedulers.mainThread()
     * @return A {@link rx.schedulers.Schedulers}.
     */
    Scheduler ui();
}