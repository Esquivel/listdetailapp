package com.fesquivel.listdetailapp.commons

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.view.View

    /**
     * Create an scale animation.
     * By [from] and [to] parameters can set an expand or contraction animation.
     *
     * @param view The [View] instance to animate.
     * @param from from value in [Float]
     * @param to to value in [Float]
     * @param duration The duration of the animation.
     * @param handleVisibility Set if handles the show or hide visibility.
     * @return Returns an [Animator] instance.
     */
    fun createScaleAnimator(view : View, from : Float, to : Float, duration : Long,
                            handleVisibility : Boolean): Animator {
        val animatorX = ObjectAnimator.ofFloat(view, View.SCALE_X, from, to)
        val animatorY = ObjectAnimator.ofFloat(view, View.SCALE_Y, from, to)
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(animatorX, animatorY)
        animatorSet.duration = duration

        if (!handleVisibility) {
            return animatorSet
        }

        setVisibilityBehaviour(animatorSet, from, to, view)

        return animatorSet
    }

    private fun setVisibilityBehaviour(animator : Animator, from : Float, to : Float, view: View) {
        val isExpandableAnimation = from < to

        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {
                super.onAnimationStart(animation)
                if (isExpandableAnimation) {
                    view.visibility = View.VISIBLE
                }
            }

            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                if (!isExpandableAnimation) {
                    view.visibility = View.INVISIBLE
                }
            }
        })
    }