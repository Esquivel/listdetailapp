package com.fesquivel.listdetailapp.commons;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

public abstract class BaseActivity<S extends BaseState, T extends BasePresenter<S>>
        extends AppCompatActivity implements Mvp.View<T> {

    private static final String STATE = "state";

    private T presenter;
    private S state;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setPresenter(onCreatePresenter());
        state = onCreateState();
    }

    /**
     * Creates the presenter.
     *
     * @return A instance of Presenter.
     */
    protected abstract T onCreatePresenter();

    protected abstract S onCreateState();

    @Override
    public void setPresenter(@NonNull final T presenter) {
        this.presenter = presenter;
    }

    @NotNull
    @Override
    public T getPresenter() {
        return presenter;
    }

    @Override
    protected void onRestoreInstanceState(@NonNull final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        state = (S) savedInstanceState.getSerializable(STATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setState(state);
        presenter.start();
    }

    @Override
    protected void onSaveInstanceState(@NonNull final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE, presenter.getState());
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.stop();
    }
}