package com.fesquivel.listdetailapp.home.repository.sale;

import androidx.annotation.WorkerThread;

import com.fesquivel.listdetailapp.home.domain.Banner;
import com.fesquivel.listdetailapp.home.domain.DisabledButton;
import com.fesquivel.listdetailapp.home.domain.Item;
import com.fesquivel.listdetailapp.home.domain.LockedButton;
import com.fesquivel.listdetailapp.home.domain.FeedSale;
import com.fesquivel.listdetailapp.home.domain.EnabledButton;
import com.fesquivel.listdetailapp.home.domain.Sale;

import java.util.ArrayList;
import java.util.List;

/**
 * Mapper. Here do all validations.
 *
 * This logic must be executed on a {@link WorkerThread}.
 */
class Mapper {

    /**
     * Mapping logic.
     *
     * @param saleApiList The object to map.
     * @return The result operation.
     */
    @WorkerThread
    FeedSale mapTo(final List<SaleDto> saleApiList) {
        return createFakeData();
    }

    /**
     * Fake data method.
     * Here creates the data to do tests.
     *
     * @return The Item list.
     */
    private FeedSale createFakeData() {
        final List<Sale> saleList = new ArrayList<>();
        saleList.add(createFirstFakeSale());
        saleList.add(createSecondFirstFakeSale());
        saleList.add(createThirdFirstFakeSale());
        saleList.add(createFourthFirstFakeSale());
        saleList.add(createFifthFirstFakeSale());
        saleList.add(createSixthFakeSale());
        saleList.add(createSevenFakeSale());

        return new InternalFeedSale.Builder("78978")
                .withSubscriptionSaleList(saleList)
                .build();
    }

    private Sale createFirstFakeSale() {
        final Item item = new InternalItem.Builder("AXCALK")
                .withTitle("Titulo corto")
                .withSubTitle("Subtitulo corto")
                .build();
        final Banner banner = new InternalBanner.Builder("-30%")
                .withSubTitle("Descuento")
                .withType(1)
                .build();
        return new InternalSale.Builder(item)
                .withButton(new DisabledButton())
                .withBanner(banner)
                .build();
    }

    private Sale createSecondFirstFakeSale() {
        final Item item = new InternalItem.Builder("DASDASDsds")
                .withTitle("Titulo extremadamente largo Titulo extremadamente largo Titulo extremadamente largo")
                .withSubTitle("Subtitulo extremadamente largo Subtitulo extremadamente largo Subtitulo extremadamente largo Subtitulo extremadamente largo")
                .build();
        final Banner banner = new InternalBanner.Builder("-10%")
                .withSubTitle("Descuento")
                .build();
        return new InternalSale.Builder(item)
                .withButton(new DisabledButton())
                .withBanner(banner)
                .build();
    }

    private Sale createThirdFirstFakeSale() {
        final Item item = new InternalItem.Builder("AXCSADsdsdALK")
                .withTitle("Titulo medianamente largo")
                .withSubTitle("Subtitulo medianamente largo")
                .build();
        final Banner banner = new InternalBanner.Builder("-35%")
                .withSubTitle("Descuento")
                .build();
        return new InternalSale.Builder(item)
                .withButton(new DisabledButton())
                .withBanner(banner)
                .build();
    }

    private Sale createFourthFirstFakeSale() {
        final Item item = new InternalItem.Builder("asdaweqDASD")
                .withTitle("Este no tiene banner")
                .withSubTitle("Sin banner")
                .build();
        return new InternalSale.Builder(item)
                .withButton(new DisabledButton())
                .build();
    }

    private Sale createFifthFirstFakeSale() {
        final Item item = new InternalItem.Builder("AXCSADsdsdALK")
                .withTitle("Sin subtitulo")
                .build();
        final Banner banner = new InternalBanner.Builder("-35%")
                .withSubTitle("Descuento")
                .build();
        return new InternalSale.Builder(item)
                .withButton(new DisabledButton())
                .withBanner(banner)
                .build();
    }

    private Sale createSixthFakeSale() {
        final Item item = new InternalItem.Builder("AXCALK")
                .withTitle("Tiene locked button")
                .withSubTitle("Tiene locked button")
                .build();
        final Banner banner = new InternalBanner.Builder("-30%")
                .withSubTitle("Descuento")
                .withType(1)
                .build();
        return new InternalSale.Builder(item)
                .withButton(new LockedButton())
                .withBanner(banner)
                .build();
    }

    private Sale createSevenFakeSale() {
        final Item item = new InternalItem.Builder("AXCALK")
                .withTitle("Boton para activar")
                .withSubTitle("Boton para activar")
                .build();
        final Banner banner = new InternalBanner.Builder("-30%")
                .withSubTitle("Descuento")
                .withType(1)
                .build();
        return new InternalSale.Builder(item)
                .withButton(new EnabledButton())
                .withBanner(banner)
                .build();
    }
}