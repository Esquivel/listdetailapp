package com.fesquivel.listdetailapp.home.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fesquivel.listdetailapp.R;

public class VerticalBannerView extends LinearLayout {

    private TextView descriptionTypeView;
    private TextView titleView;
    private TextView subTitleView;

    public VerticalBannerView(@NonNull final Context context) {
        super(context);
        init(context);
    }

    public VerticalBannerView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public VerticalBannerView(@NonNull final Context context, @Nullable final AttributeSet attrs,
                              final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(final Context context) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View container = inflater.inflate(R.layout.vertical_banner_view_layout, this);

        setOrientation(VERTICAL);
        setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));
        setFocusable(true);

        descriptionTypeView = container.findViewById(R.id.descriptionType);
        titleView = container.findViewById(R.id.title);
        subTitleView = container.findViewById(R.id.subTitle);
    }

    public void setTitleView(final String title) {
        titleView.setText(title);
    }

    public void setSubTitleView(final String subTitle) {
        subTitleView.setText(subTitle);
    }

    public void setDescriptionTypeView(@NonNull final String description) {
        descriptionTypeView.setVisibility(description.isEmpty() ? INVISIBLE : VISIBLE);
        descriptionTypeView.setText(description);
    }
}
