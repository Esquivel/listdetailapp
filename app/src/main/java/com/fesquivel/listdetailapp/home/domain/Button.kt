package com.fesquivel.listdetailapp.home.domain

import java.io.Serializable

interface Button: Serializable {

    /**
     * Get the [Button] type.
     *
     * @return The button type.
     */
    fun getType(): Int

    /**
     * Get the [Int] to the related resource.
     *
     * @return The related resource.
     */
    fun getResId(): Int
}