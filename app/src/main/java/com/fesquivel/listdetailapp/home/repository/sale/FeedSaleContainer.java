package com.fesquivel.listdetailapp.home.repository.sale;

import com.fesquivel.listdetailapp.commons.SchedulerProvider;
import com.fesquivel.listdetailapp.home.domain.FeedSale;

public class FeedSaleContainer {

    private FeedSaleApi api;
    private SaleRepository repository;
    private Mapper mapper;
    private SchedulerProvider schedulerProvider;

    public SaleRepository provideRepository() {
        if (repository == null) {
            repository = new FeedSaleRepository(provideApi(), provideSchedulerProvider(),
                    provideMapper());
        }
        return repository;
    }

    public FeedSale provideSaleUser() {
        return new InternalFeedSale.Builder("").build();
    }

    private FeedSaleApi provideApi() {
        if (api == null) {
            api = new FakeApiFeed();
        }
        return api;
    }

    private Mapper provideMapper() {
        if (mapper == null) {
            mapper = new Mapper();
        }
        return mapper;
    }

    private SchedulerProvider provideSchedulerProvider() {
        if (schedulerProvider == null) {
            schedulerProvider = new SchedulerProvider();
        }
        return schedulerProvider;
    }
}