package com.fesquivel.listdetailapp.home.repository.sale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fesquivel.listdetailapp.home.domain.Banner;
import com.fesquivel.listdetailapp.home.domain.Button;
import com.fesquivel.listdetailapp.home.domain.Item;
import com.fesquivel.listdetailapp.home.domain.Sale;

final class InternalSale implements Sale {

    private static final long serialVersionUID = 4503663721419033170L;

    private final Item item;
    private final Banner banner;
    private Button button;

    InternalSale(final Builder builder) {
        item = builder.item;
        banner = builder.banner;
        button = builder.button;
    }

    @Override
    public Item getItem() {
        return item;
    }

    @Override
    public Button getButton() {
        return button;
    }

    @Override
    public void setButton(final Button button) {
        this.button = button;
    }

    @Nullable
    @Override
    public Banner getBanner() {
        return banner;
    }

    public static final class Builder {

        final Item item;
        Button button;
        Banner banner;

        public Builder(final Item item) {
            this.item = item;
        }

        public Builder withButton(final Button button) {
            this.button = button;
            return this;
        }

        public Builder withBanner(final Banner banner) {
            this.banner = banner;
            return this;
        }

        @NonNull
        public Sale build() {
            return new InternalSale(this);
        }
    }
}