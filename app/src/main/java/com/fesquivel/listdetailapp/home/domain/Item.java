package com.fesquivel.listdetailapp.home.domain;

import androidx.annotation.NonNull;

import java.io.Serializable;

public interface Item extends Serializable {

    /**
     * Gets the code.
     *
     * @return The code.
     */
    @NonNull
    String getCode();

    /**
     * Get the image. Could be null.
     *
     * @return Returns the Uri with the image.
     */
    String getImage();

    /**
     * Get title text.
     *
     * @return The title text. Could be null.
     */
    String getTitle();

    /**
     * Get subtitle text. Could be null.
     *
     * @return The subtitle.
     */
    String getSubTitle();
}