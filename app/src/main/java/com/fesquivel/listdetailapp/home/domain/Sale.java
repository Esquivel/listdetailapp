package com.fesquivel.listdetailapp.home.domain;

import androidx.annotation.Nullable;

import java.io.Serializable;

public interface Sale extends Serializable {

    /**
     * Get the {@link Item} information.
     *
     * @return The {@link Item} information.
     */
    Item getItem();

    /**
     * Get the {@link Button} information.
     *
     * @return The {@link Button} information.
     */
    Button getButton();


    /**
     * Set the {@link Button} information.
     *
     * @param button The {@link Button} information.
     */
    void setButton(final Button button);

    /**
     * Get the {@link Banner} information.
     *
     * @return The {@link Banner} information.
     */
    @Nullable
    Banner getBanner();
}