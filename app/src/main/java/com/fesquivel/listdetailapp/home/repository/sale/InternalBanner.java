package com.fesquivel.listdetailapp.home.repository.sale;

import androidx.annotation.NonNull;

import com.fesquivel.listdetailapp.home.domain.Banner;

class InternalBanner implements Banner {

    private static final long serialVersionUID = 6487709430749847205L;

    private final String title;
    private final String subTitle;
    private final Integer type;

    InternalBanner(final Builder builder) {
        title = builder.title;
        subTitle = builder.subTitle;
        type = builder.type;
    }

    @NonNull
    @Override
    public String getTitle() {
        return title;
    }

    @NonNull
    @Override
    public String getSubtitle() {
        return subTitle;
    }

    @NonNull
    @Override
    public Integer getType() {
        return type;
    }

    static class Builder {

        final String title;
        String subTitle;
        Integer type;

        Builder(final String title) {
            this.title = title;
        }

        public Builder withSubTitle(final String subTitle) {
            this.subTitle = subTitle;
            return this;
        }

        public Builder withType(final Integer type) {
            this.type = type;
            return this;
        }

        @NonNull
        public Banner build() {
            if (type == null) {
                type = 0;
            }

            return new InternalBanner(this);
        }
    }
}