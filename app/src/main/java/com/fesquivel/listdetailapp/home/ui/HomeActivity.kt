package com.fesquivel.listdetailapp.home.ui

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.fesquivel.listdetailapp.R
import com.fesquivel.listdetailapp.commons.BaseActivity
import com.fesquivel.listdetailapp.commons.SchedulerProvider
import com.fesquivel.listdetailapp.detail.ui.DetailActivity
import com.fesquivel.listdetailapp.home.domain.Banner
import com.fesquivel.listdetailapp.home.domain.Item
import com.fesquivel.listdetailapp.home.domain.Sale
import com.fesquivel.listdetailapp.home.repository.sale.FeedSaleContainer
import com.fesquivel.listdetailapp.home.repository.user.UserContainer
import com.fesquivel.listdetailapp.home.ui.view.VoucherView
import io.reactivex.disposables.CompositeDisposable

private const val USER_ID_KEY = "user_id"
private const val ITEM_ID_KEY = "item"
private const val BANNER_ID_KEY = "banner_id"

class HomeActivity : BaseActivity<HomeState, HomePresenter>(), HomeView<HomePresenter> {

    private lateinit var adapter: HomeAdapter
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var voucherView: VoucherView
    private val feedSaleContainer = FeedSaleContainer()
    private val userContainer = UserContainer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity_layout)
        setToolbar()
        adapter = HomeAdapter()
        adapter.setCallback(object : HomeAdapter.Callback {
            override fun onButtonClicked(sale: Sale) {
                getPresenter().onItemButtonClicked(sale)
            }

            override fun onContainerClicked(sale: Sale) {
                getPresenter().onCardClicked(sale)
            }
        })

        voucherView = findViewById(R.id.voucherView)

        val recyclerView = findViewById<RecyclerView>(R.id.recycler)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        swipeRefreshLayout = findViewById(R.id.swipeRefresh)
        swipeRefreshLayout.setOnRefreshListener { getPresenter().onRefresh() }
    }

    override fun onCreatePresenter(): HomePresenter {
        return HomePresenter(
            feedSaleContainer.provideRepository(),
            userContainer.provideUserRepository(this),
            SchedulerProvider(),
            CompositeDisposable())
    }

    override fun onCreateState(): HomeState {
        return HomeState(feedSaleContainer.provideSaleUser(), null)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val view = super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.home_menu, menu)
        return view
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.refresh) {
            getPresenter().onRefresh()
        }
        if (item.itemId == R.id.camera) {
            getPresenter().onCameraClicked()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        getPresenter().view = this
    }

    override fun onStop() {
        super.onStop()
        getPresenter().view = null
    }

    override fun showData(dataList: List<Sale>) {
        adapter.setData(dataList)
    }

    override fun showLoading(enabled: Boolean) {
        if (swipeRefreshLayout.isRefreshing == enabled) {
            return
        }

        swipeRefreshLayout.isRefreshing = enabled
    }

    override fun showVoucherQuantity(quantity: Int) {
        voucherView.setVoucherQuantity(quantity)
    }

    override fun startDetailScreen(userId: String, item: Item, banner: Banner?) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(USER_ID_KEY, userId)
        intent.putExtra(ITEM_ID_KEY, item)
        intent.putExtra(BANNER_ID_KEY, banner)
        startActivity(intent)
    }

    override fun startCameraScreen() {
        Toast.makeText(this, getString(R.string.camera_message), Toast.LENGTH_SHORT).show()
    }

    override fun showLoginDialog() {
        AlertDialog.Builder(this)
            .setMessage(getString(R.string.dialog_login_message))
            .setPositiveButton(getString(android.R.string.ok))
            { _, _ -> getPresenter().onLoginButtonClicked() }
            .create()
            .show()
    }

    private fun setToolbar() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = null
    }
}