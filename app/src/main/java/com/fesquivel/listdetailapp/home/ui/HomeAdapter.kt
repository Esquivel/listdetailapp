package com.fesquivel.listdetailapp.home.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.fesquivel.listdetailapp.R
import com.fesquivel.listdetailapp.commons.ButtonType
import com.fesquivel.listdetailapp.home.domain.Banner
import com.fesquivel.listdetailapp.home.domain.Button
import com.fesquivel.listdetailapp.home.domain.Item
import com.fesquivel.listdetailapp.home.domain.Sale
import com.fesquivel.listdetailapp.home.ui.view.VerticalBannerView
import com.squareup.picasso.Picasso

class HomeAdapter: RecyclerView.Adapter<HomeAdapter.ItemView>() {

    private var list = ArrayList<Sale>()
    private var callback: Callback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemView {
        return ItemView(LayoutInflater.from(parent.context).inflate(R.layout.sale_item_layout,
            parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ItemView, position: Int) {
        val sale = list[position]

        holder.containerView.setOnClickListener { callback?.onContainerClicked(sale) }

        val context = holder.imageView.context
        Picasso.with(context)
            .load(sale.item.image)
            .placeholder(ContextCompat.getDrawable(context, R.color.colorAccent))
            .fit()
            .centerInside()
            .into(holder.imageView)

        showTitleSubtitle(sale.item, holder)
        showBannerIfNeeded(sale.banner, holder.bannerView)
        showButton(sale, holder)
    }

    fun setData(list: List<Sale>) {
        this.list.clear()
        this.list.addAll(list)

        notifyDataSetChanged()
    }

    fun setCallback(callback: Callback) {
        this.callback = callback
    }

    private fun showTitleSubtitle(item: Item, holder: ItemView) {
        holder.titleView.text = item.title
        holder.subTitleView.text = item.subTitle
    }

    private fun showButton(sale: Sale, holder: ItemView) {
        val button = sale.button
        when (button.getType()) {
            ButtonType.LOCKED -> {
                showIconButton(button, holder)
            }
            ButtonType.ENABLED -> {
                showEnabledButton(button, holder)
            }
            else -> {
                showDisabledButton(button, holder)
            }
        }

        holder.buttonTypeView.setOnClickListener { callback?.onButtonClicked(sale) }
    }

    private fun showBannerIfNeeded(banner: Banner?, bannerView: VerticalBannerView) {
        if (banner != null) {
            bannerView.visibility = View.VISIBLE
            showBanner(banner, bannerView)
        } else {
            bannerView.visibility = View.GONE
        }
    }

    private fun showBanner(banner: Banner, bannerView: VerticalBannerView) {
        bannerView.setDescriptionTypeView(if (banner.type == 1) "Texto" else "")
        bannerView.setTitleView(banner.title)
        bannerView.setSubTitleView(banner.subtitle)
    }

    private fun showEnabledButton(button: Button, holder: ItemView) {
        showTextButton(button, holder, R.color.colorSubtitleGreen)
    }

    private fun showDisabledButton(button: Button, holder: ItemView) {
        showTextButton(button, holder, R.color.colorPrimaryDark)
    }

    private fun showTextButton(button: Button, holder: ItemView, color: Int) {
        holder.imageTypeView.visibility = View.GONE

        val buttonView = holder.buttonTypeView
        buttonView.visibility = View.VISIBLE
        buttonView.text = buttonView.context.getString(button.getResId())
        buttonView.background = ContextCompat.getDrawable(buttonView.context, color)
    }

    private fun showIconButton(button: Button, holder: ItemView) {
        holder.buttonTypeView.visibility = View.GONE

        val imageView = holder.imageTypeView
        imageView.visibility = View.VISIBLE
        Picasso.with(imageView.context)
            .load(button.getResId())
            .placeholder(R.drawable.ic_launcher_background)
            .fit()
            .centerInside()
            .into(imageView)
    }

    class ItemView(itemView: View): RecyclerView.ViewHolder(itemView) {
        val imageView: ImageView = itemView.findViewById(R.id.image)
        val bannerView: VerticalBannerView = itemView.findViewById(R.id.bannerView)
        val titleView: TextView = itemView.findViewById(R.id.saleTitle)
        val subTitleView: TextView = itemView.findViewById(R.id.saleSubTitle)
        val buttonTypeView: TextView = itemView.findViewById(R.id.buttonType)
        val imageTypeView: ImageView = itemView.findViewById(R.id.imageType)
        val containerView: View = itemView.findViewById(R.id.container)
    }

    interface Callback {

        fun onButtonClicked(sale: Sale)

        fun onContainerClicked(sale: Sale)
    }
}