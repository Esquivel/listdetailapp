package com.fesquivel.listdetailapp.home.repository.user;

import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

class DataUserRepository implements UserRepository {

    private static final String DUMMY_USER_ID = "2203";
    private static final String USER_KEY = "user_key";

    private final SharedPreferences locationPref;
    private final Gson gson;

    DataUserRepository(final SharedPreferences locationPref, final Gson gson) {
        this.locationPref = locationPref;
        this.gson = gson;
    }

    @Override
    public User get() {
        User user = gson.fromJson(locationPref.getString(USER_KEY, null),
                InternalUser.class);

        if (user == null) {
            user = new InternalUser(DUMMY_USER_ID);
        }

        return user;
    }

    @Override
    public void save(@NonNull final User user) {
        final SharedPreferences.Editor editor = locationPref.edit();

        editor.putString(USER_KEY, gson.toJson(user));
        editor.apply();
    }

    static class InternalUser implements User {

        private static final long serialVersionUID = -2779809460869138768L;

        private final String id;

        InternalUser(final String id) {
            this.id = id;
        }

        @NonNull
        @Override
        public String getUserId() {
            return id;
        }
    }
}
