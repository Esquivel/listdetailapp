package com.fesquivel.listdetailapp.home.repository.sale;

import androidx.annotation.NonNull;

import com.fesquivel.listdetailapp.home.domain.FeedSale;
import com.fesquivel.listdetailapp.home.domain.Sale;

import io.reactivex.Observable;

public interface SaleRepository {

    /**
     * Get the information related to {@link FeedSale}. Could return a cache saved.
     *
     * @param userId The user id.
     * @param force Force to refresh data.
     */
    Observable<FeedSale> get(final String userId, final boolean force);

    /**
     * Save the item selected.
     *
     * @param sale The Sale to save.
     * @param userId The user id.
     */
    Observable<String> saveItem(@NonNull final Sale sale, final String userId);
}