package com.fesquivel.listdetailapp.home.ui

import com.fesquivel.listdetailapp.commons.Mvp
import com.fesquivel.listdetailapp.home.domain.Banner
import com.fesquivel.listdetailapp.home.domain.Item
import com.fesquivel.listdetailapp.home.domain.Sale

interface HomeView<T: Mvp.Presenter>: Mvp.View<T> {

    /**
     * Show a dialog.
     */
    fun showLoginDialog()

    /**
     * Shows the data.
     * 
     * @param dataList The data to show.
     */
    fun showData(dataList: List<Sale>)

    /**
     * Shows a loading.
     *
     * @param enabled If is enabled to show.
     */
    fun showLoading(enabled: Boolean)

    /**
     * Shows the voucher quantity.
     *
     * @param quantity The quantity to show.
     */
    fun showVoucherQuantity(quantity: Int)

    /**
     * Starts the detail screen.
     *
     * @param userId The user id.
     * @param item The item data.
     * @param banner The banner.
     */
    fun startDetailScreen(userId: String, item: Item, banner: Banner?)

    /**
     * Starts the camera screen.
     */
    fun startCameraScreen()
}