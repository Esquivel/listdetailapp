package com.fesquivel.listdetailapp.home.domain;

import androidx.annotation.NonNull;

import java.io.Serializable;

public interface Banner extends Serializable {

    /**
     * Get the banner title.
     *
     * @return The banner title.
     */
    @NonNull
    String getTitle();

    /**
     * Get the banner subtitle.
     *
     * @return The banner subtitle.
     */
    @NonNull
    String getSubtitle();

    /**
     * Get the banner type.
     *
     * @return The banner type.
     */
    @NonNull
    Integer getType();
}
