package com.fesquivel.listdetailapp.home.repository.user;

import androidx.annotation.NonNull;

import java.io.Serializable;

public interface User extends Serializable {

    /**
     * Get the identification user code.
     *
     * @return The id.
     */
    @NonNull
    String getUserId();
}