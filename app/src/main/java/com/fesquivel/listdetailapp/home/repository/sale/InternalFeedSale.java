package com.fesquivel.listdetailapp.home.repository.sale;

import androidx.annotation.NonNull;

import com.fesquivel.listdetailapp.home.domain.FeedSale;
import com.fesquivel.listdetailapp.home.domain.Sale;

import java.util.Collections;
import java.util.List;

class InternalFeedSale implements FeedSale {

    private static final long serialVersionUID = 4545365519452363343L;

    @NonNull
    private final String userId;
    @NonNull
    private final List<Sale> subscriptionSaleList;
    @NonNull
    private Integer voucherQuantity;

    InternalFeedSale(@NonNull final String userId, @NonNull final List<Sale> subscriptionSaleList,
                     @NonNull final Integer voucherQuantity) {
        this.userId = userId;
        this.subscriptionSaleList = subscriptionSaleList;
        this.voucherQuantity = voucherQuantity;
    }

    @NonNull
    @Override
    public String getUserId() {
        return userId;
    }

    @NonNull
    @Override
    public List<Sale> getSaleList() {
        return subscriptionSaleList;
    }

    @NonNull
    @Override
    public Integer getVoucherQuantity() {
        return voucherQuantity;
    }

    @Override
    public void setVoucherQuantity(@NonNull final Integer quantity) {
        voucherQuantity = quantity;
    }

    static class Builder {

        final String userId;
        List<Sale> subscriptionSaleList;
        Integer voucherQuantity;

        Builder(@NonNull final String userId) {
            this.userId = userId;
        }

        Builder withSubscriptionSaleList(final List<Sale> subscriptionSaleList) {
            this.subscriptionSaleList = subscriptionSaleList;
            return this;
        }

        Builder withVoucherQuantity(final Integer voucherQuantity) {
            this.voucherQuantity = voucherQuantity;
            return this;
        }

        FeedSale build() {
            if (subscriptionSaleList == null) {
                subscriptionSaleList = Collections.emptyList();
            }

            if (voucherQuantity == null) {
                voucherQuantity = 0;
            }

            return new InternalFeedSale(userId, subscriptionSaleList, voucherQuantity);
        }
    }
}