package com.fesquivel.listdetailapp.home.domain

import com.fesquivel.listdetailapp.R
import com.fesquivel.listdetailapp.commons.ButtonType

class LockedButton: Button {

    override fun getType(): Int {
        return ButtonType.LOCKED
    }

    override fun getResId(): Int {
        return R.drawable.ic_launcher_background
    }
}