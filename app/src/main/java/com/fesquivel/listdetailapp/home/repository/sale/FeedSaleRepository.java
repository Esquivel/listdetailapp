package com.fesquivel.listdetailapp.home.repository.sale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fesquivel.listdetailapp.commons.BaseSchedulerProvider;
import com.fesquivel.listdetailapp.home.domain.FeedSale;
import com.fesquivel.listdetailapp.home.domain.Sale;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

class FeedSaleRepository implements SaleRepository {

    private static final int SIMULATION_TIME_RESPONSE = 3000;

    private final FeedSaleApi api;
    private final BaseSchedulerProvider schedulerProvider;
    private final Mapper mapper;

    @Nullable
    private FeedSale cache;

    FeedSaleRepository(final FeedSaleApi api, final BaseSchedulerProvider schedulerProvider,
                       final Mapper mapper) {
        this.api = api;
        this.schedulerProvider = schedulerProvider;
        this.mapper = mapper;
    }

    @Override
    public Observable<FeedSale> get(final String userId, final boolean force) {
        if (force) {
            cache = null;
        }

        if (cache != null) {
            return Observable.just(cache);
        }

        return api.get(userId)
                .map(mapper::mapTo)
                .map(this::saveToCache)
                .subscribeOn(schedulerProvider.io())
                .delaySubscription(SIMULATION_TIME_RESPONSE, TimeUnit.MILLISECONDS);
    }

    @Override
    public Observable<String> saveItem(@NonNull final Sale sale, final String userId) {
        return api.save(sale)
                .subscribeOn(schedulerProvider.io())
                .delaySubscription(SIMULATION_TIME_RESPONSE, TimeUnit.MILLISECONDS);
    }

    private FeedSale saveToCache(final FeedSale feedSale) {
        cache = feedSale;
        return cache;
    }
}