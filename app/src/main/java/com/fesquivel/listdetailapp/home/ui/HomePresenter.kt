package com.fesquivel.listdetailapp.home.ui

import com.fesquivel.listdetailapp.commons.BasePresenter
import com.fesquivel.listdetailapp.commons.BaseSchedulerProvider
import com.fesquivel.listdetailapp.commons.ButtonType
import com.fesquivel.listdetailapp.home.domain.*
import com.fesquivel.listdetailapp.home.repository.sale.SaleRepository
import com.fesquivel.listdetailapp.home.repository.user.UserRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class HomePresenter(private val saleRepository: SaleRepository,
                    private val userRepository: UserRepository,
                    private val schedulerProvider: BaseSchedulerProvider,
                    private val compositeDisposable: CompositeDisposable
) : BasePresenter<HomeState>() {

    private lateinit var disposable: Disposable
    var view : HomeView<HomePresenter>? = null

    override fun start() {
        getRefreshData(false)
    }

    override fun stop() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    /**
     * Called when occurs a refresh action.
     */
    fun onRefresh() {
        getRefreshData(true)
    }

    /**
     * Called when user click on a card.
     */
    fun onCardClicked(sale: Sale) {
        view?.startDetailScreen(state.user!!.userId, sale.item, sale.banner)
    }

    /**
     * Called when user click a card button.
     */
    fun onItemButtonClicked(sale: Sale) {
        if (!isEnabledButton(sale.button)) {
            //TODO At this point could add tracks to understand the user behaviour.
            return
        }

        updateButton(sale, state.feedSale.saleList)
        updateTotalVoucher(state.feedSale)
        showData(state.feedSale)

        disposable = saleRepository.saveItem(sale, state.user?.userId)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe(
                {
                    showLoading(false)
                    getRefreshData(true)
                } , {
                    showLoading(false)
                })

        compositeDisposable.add(disposable)
    }

    /**
     * Called when user click login button.
     */
    fun onLoginButtonClicked() {
        state.user = userRepository.get()
        getFeedSaleData(false)
    }

    private fun isEnabledButton(button: Button): Boolean {
        return button.getType() == ButtonType.ENABLED
    }

    private fun updateButton(sale: Sale, list: List<Sale>) {
        val indexToUpdate = list.indexOf(sale)
        list[indexToUpdate].button = DisabledButton()
    }

    private fun updateTotalVoucher(feedSale: FeedSale) {
        feedSale.voucherQuantity++
    }

    private fun getRefreshData(forceRefresh: Boolean) {
        when {
            isInvalidUser() -> {
                showLoginDialog()
            }
            forceRefresh -> {
                getFeedSaleData(true)
            }
            hasFeedSaleToShow() -> {
                showData(state.feedSale)
            }
            else -> {
                getFeedSaleData(forceRefresh)
            }
        }
    }

    private fun showData(feedSale: FeedSale) {
        showLoading(false)

        view?.showData(feedSale.saleList)
        view?.showVoucherQuantity(feedSale.voucherQuantity)
    }

    private fun showLoading(enabled: Boolean) {
        view?.showLoading(enabled)
    }

    private fun showLoginDialog() {
        showLoading(false)
        view?.showLoginDialog()
    }

    private fun isInvalidUser(): Boolean {
        return state.user == null || state.user!!.userId.isEmpty()
    }

    private fun hasFeedSaleToShow(): Boolean {
        return state.feedSale.saleList.isNotEmpty()
    }

    private fun getFeedSaleData(forceRefresh: Boolean) {
        showLoading(true)

        disposable = saleRepository.get(state.user?.userId, forceRefresh)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe(
                { response ->
                    state.feedSale = response
                    showData(state.feedSale)
                }, {
                    showLoading(false)
                })

        compositeDisposable.add(disposable)
    }

    fun onCameraClicked() {
        view?.startCameraScreen()
    }
}