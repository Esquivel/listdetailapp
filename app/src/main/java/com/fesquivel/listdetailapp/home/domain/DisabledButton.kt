package com.fesquivel.listdetailapp.home.domain

import com.fesquivel.listdetailapp.R
import com.fesquivel.listdetailapp.commons.ButtonType

class DisabledButton: Button {

    override fun getType(): Int {
        return ButtonType.DISABLED
    }

    override fun getResId(): Int {
        return R.string.button_disabled_text
    }
}