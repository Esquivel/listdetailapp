package com.fesquivel.listdetailapp.home.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.fesquivel.listdetailapp.R;
import com.fesquivel.listdetailapp.commons.AnimationUtilsKt;

public class VoucherView extends ConstraintLayout {

    private static final int ANIMATION_DURATION = 500;

    private TextView subtitleView;

    public VoucherView(@NonNull final Context context) {
        super(context);
        init(context);
    }

    public VoucherView(@NonNull final Context context, @Nullable final  AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public VoucherView(@NonNull final Context context, @Nullable final AttributeSet attrs,
                       final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public VoucherView(@NonNull final Context context, @Nullable final AttributeSet attrs,
                       final int defStyleAttr, final int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(final Context context) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View container = inflater.inflate(R.layout.voucher_view_layout, this);

        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        setFocusable(true);

        subtitleView = container.findViewById(R.id.subtitle);
    }

    /**
     * Set the quantity vouchers to display to the user.
     *
     * @param quantity The quantity
     */
    public void setVoucherQuantity(final int quantity) {
        subtitleView.setVisibility(VISIBLE);

        final int colorVoucher = quantity == 0
                ? R.color.colorSubtitleRed : R.color.colorSubtitleGreen;
        subtitleView.setTextColor(ContextCompat.getColor(getContext(), colorVoucher));
        subtitleView.setText(getResources().getString(R.string.voucher_message,
                String.valueOf(quantity)));

        AnimationUtilsKt.createScaleAnimator(subtitleView, 0f, 1f,
                ANIMATION_DURATION, true).start();
    }
}