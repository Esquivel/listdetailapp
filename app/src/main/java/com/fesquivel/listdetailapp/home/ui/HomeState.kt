package com.fesquivel.listdetailapp.home.ui

import com.fesquivel.listdetailapp.commons.BaseState
import com.fesquivel.listdetailapp.home.domain.FeedSale
import com.fesquivel.listdetailapp.home.repository.user.User

class HomeState(var feedSale: FeedSale, var user: User?): BaseState()