package com.fesquivel.listdetailapp.home.repository.sale;

import androidx.annotation.NonNull;

import com.fesquivel.listdetailapp.home.domain.Item;

public class InternalItem implements Item {

    private static final long serialVersionUID = -1527793224446894829L;

    private final String code;
    private final String image;
    private final String title;
    private final String subTitle;

    public InternalItem(final Builder builder) {
        code = builder.code;
        image = builder.image;
        title = builder.title;
        subTitle = builder.subTitle;
    }

    @NonNull
    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getImage() {
        return image;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSubTitle() {
        return subTitle;
    }

    public static final class Builder {

        String code;
        String image;
        String title;
        String subTitle;

        public Builder(final String code) {
            this.code = code;
        }

        public Builder withImage(final String image) {
            this.image = image;
            return this;
        }

        public Builder withTitle(final String title) {
            this.title = title;
            return this;
        }

        public Builder withSubTitle(final String subTitle) {
            this.subTitle = subTitle;
            return this;
        }

        @NonNull
        public Item build() {
            return new InternalItem(this);
        }
    }
}