package com.fesquivel.listdetailapp.home.repository.sale;

import com.fesquivel.listdetailapp.home.domain.Sale;

import java.util.List;

import io.reactivex.Observable;

/**
 * This interface simulates the Retrofit interface to do an Api request.
 */
public interface FeedSaleApi {

    /**
     * @GET("user/")
     * @Query("user_id") final String userId;
     *
     * @param userId The user id.
     * @return The list sale dto wrapped on a Observable.
     */
    Observable<List<SaleDto>> get(final String userId);

    /**
     * @POST("user/")
     * @Query("item") final Sale sale;
     *
     * @param sale The sale to save.
     * @return The list sale dto wrapped on a Observable.
     */
    Observable<String> save(final Sale sale);
}