package com.fesquivel.listdetailapp.home.domain

import com.fesquivel.listdetailapp.R
import com.fesquivel.listdetailapp.commons.ButtonType

class EnabledButton: Button {

    override fun getType(): Int {
        return ButtonType.ENABLED
    }

    override fun getResId(): Int {
        return R.string.button_enabled_text
    }
}