package com.fesquivel.listdetailapp.home.repository.sale;

class SaleDto {

    public final String image;
    public final String overImage;
    public final String title;
    public final String subTitle;
    public final Integer button;

    SaleDto(final String image, final String overImage, final String title,
                   final String subTitle, final Integer button) {
        this.image = image;
        this.overImage = overImage;
        this.title = title;
        this.subTitle = subTitle;
        this.button = button;
    }
}