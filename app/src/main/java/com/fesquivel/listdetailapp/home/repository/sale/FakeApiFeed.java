package com.fesquivel.listdetailapp.home.repository.sale;

import com.fesquivel.listdetailapp.home.domain.Sale;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;

class FakeApiFeed implements FeedSaleApi {

    @Override
    public Observable<List<SaleDto>> get(final String userId) {
        return Observable.just(Collections.emptyList());
    }

    @Override
    public Observable<String> save(final Sale sale) {
        return Observable.just("");
    }
}
