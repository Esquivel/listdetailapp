package com.fesquivel.listdetailapp.home.domain;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.List;

/**
 * All information related to sale.
 */
public interface FeedSale extends Serializable {

    /**
     * Get the identification user code.
     *
     * @return The id.
     */
    @NonNull
    String getUserId();

    /**
     * Returns a list with the items that this user is subscribed or are active.
     *
     * @return The list with the items.
     */
    @NonNull
    List<Sale> getSaleList();

    /**
     * Returns the voucher quantity.
     *
     * @return The quantity.
     */
    @NonNull
    Integer getVoucherQuantity();

    /**
     * Set the voucher quantity.
     *
     * @param quantity The quantity.
     */
    void setVoucherQuantity(@NonNull Integer quantity);
}
