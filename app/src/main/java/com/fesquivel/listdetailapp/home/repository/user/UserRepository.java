package com.fesquivel.listdetailapp.home.repository.user;

import androidx.annotation.NonNull;

public interface UserRepository {

    /**
     * Get the information related to {@link User}.
     */
    User get();

    /**
     * Save changes related to the user.
     */
    void save(@NonNull final User t);
}