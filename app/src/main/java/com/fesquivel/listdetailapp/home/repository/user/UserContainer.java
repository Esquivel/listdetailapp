package com.fesquivel.listdetailapp.home.repository.user;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

public class UserContainer {

    private static final String USER_SHARED_PREF = "user_shared_pref";

    private UserRepository repository;
    private Gson gson;
    private SharedPreferences locationPref;

    public UserRepository provideUserRepository(final Context context) {
        if (repository == null) {
            repository = new DataUserRepository(provideSharedPreferences(context), provideGson());
        }
        return repository;
    }

    private Gson provideGson() {
        if (gson == null) {
            gson = new Gson();
        }
        return new Gson();
    }

    private SharedPreferences provideSharedPreferences(final Context context) {
        if (locationPref == null) {
            locationPref = context.getSharedPreferences(USER_SHARED_PREF, 0);
        }
        return locationPref;
    }
}