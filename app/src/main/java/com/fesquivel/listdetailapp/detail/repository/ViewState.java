package com.fesquivel.listdetailapp.detail.repository;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@IntDef({
        ViewState.LOADING,
        ViewState.READY
})
public @interface ViewState {
    int LOADING = 0;
    int READY = 1;
}