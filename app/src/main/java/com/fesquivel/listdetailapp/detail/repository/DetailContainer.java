package com.fesquivel.listdetailapp.detail.repository;

import com.fesquivel.listdetailapp.commons.SchedulerProvider;
import com.fesquivel.listdetailapp.detail.domain.Detail;

import java.util.List;

public class DetailContainer {

    private DetailRepository<List<Detail>> repository;
    private DetailMapper mapper;
    private DetailApi api;
    private SchedulerProvider schedulerProvider;
    private RepositoryStateHandler stateHandler;

    public DetailRepository<List<Detail>> provideRepository() {
        if (repository == null) {
            repository = new SaleDetailRepository(provideApi(), provideSchedulerProvider(),
                    provideMapper(), provideStateHandler());
        }
        return repository;
    }

    private DetailApi provideApi() {
        if (api == null) {
            api = new FakeDetailApi();
        }
        return api;
    }

    private SchedulerProvider provideSchedulerProvider() {
        if (schedulerProvider == null) {
            schedulerProvider = new SchedulerProvider();
        }
        return schedulerProvider;
    }

    private DetailMapper provideMapper() {
        if (mapper == null) {
            mapper = new DetailMapper();
        }
        return mapper;
    }

    private RepositoryStateHandler provideStateHandler() {
        if (stateHandler == null) {
            stateHandler = new RepositoryStateHandler();
        }
        return stateHandler;
    }
}