package com.fesquivel.listdetailapp.detail.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.fesquivel.listdetailapp.detail.domain.Detail
import com.fesquivel.listdetailapp.detail.repository.DetailRepository
import com.fesquivel.listdetailapp.detail.repository.State
import com.fesquivel.listdetailapp.home.domain.Banner
import com.fesquivel.listdetailapp.home.domain.Item
import com.fesquivel.listdetailapp.home.repository.user.User
import com.fesquivel.listdetailapp.home.repository.user.UserRepository

private const val USER_ID_KEY = "user_id"
private const val ITEM_ID_KEY = "item"
private const val BANNER_ID_KEY = "banner_id"

class DetailViewModel(
    state: SavedStateHandle,
    private val detailRepository: DetailRepository<List<Detail>>
) : ViewModel() {

    val item : LiveData<Item> = MutableLiveData(state[ITEM_ID_KEY])
    val userId : LiveData<String> = MutableLiveData(state[USER_ID_KEY])
    val banner : LiveData<Banner?> = MutableLiveData(state[BANNER_ID_KEY])
    val details : LiveData<List<Detail>> = getData()
    val loadingState : LiveData<State> = detailRepository.state

    fun onRefreshData() {
        getData()
    }

    private fun getData() : LiveData<List<Detail>> {
        return detailRepository.get(userId.value, item.value!!.code)
    }
}
