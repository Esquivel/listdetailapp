package com.fesquivel.listdetailapp.detail.domain;

import java.io.Serializable;
import java.util.List;

public class SaleDetail implements Serializable {

    private static final long serialVersionUID = -5103932027900617925L;

    private String header;
    private List<Detail> detailList;

    public String getHeader() {
        return header;
    }

    public void setHeader(final String header) {
        this.header = header;
    }

    public List<Detail> getDetailList() {
        return detailList;
    }

    public void setDetailList(final List<Detail> detailList) {
        this.detailList = detailList;
    }
}
