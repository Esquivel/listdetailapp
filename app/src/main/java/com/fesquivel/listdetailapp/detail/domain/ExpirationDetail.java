package com.fesquivel.listdetailapp.detail.domain;

public class ExpirationDetail implements Detail {

    private static final long serialVersionUID = -8434387124852375114L;

    private final Integer validDays;

    public ExpirationDetail(final Integer validDays) {
        this.validDays = validDays;
    }

    @Override
    public int getType() {
        return DetailType.EXPIRATION;
    }

    public Integer getValidDays() {
        return validDays;
    }
}
