package com.fesquivel.listdetailapp.detail.repository

/**
 * This interface could used to notifies the state of repository.
 */
interface State {

    /**
     * Get the View State.
     */
    @ViewState
    fun getViewState() : Int
}