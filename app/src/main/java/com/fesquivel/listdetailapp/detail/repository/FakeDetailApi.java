package com.fesquivel.listdetailapp.detail.repository;

import com.fesquivel.listdetailapp.detail.domain.DetailType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;

/**
 * Simulates the server response.
 */
class FakeDetailApi implements DetailApi {

    @Override
    public Observable<List<DetailDto>> get(final String userId, final String saleId) {
        return Observable.just(createTestData());
    }

    /**
     * Creates data test.
     * Has invalid data, and valid repeated to show a scroll behaviour.
     * After creation of list, do a shuffle.
     *
     * @return The test data.
     */
    private List<DetailDto> createTestData() {
        final List<DetailDto> detailDtoList = new ArrayList<>();

        detailDtoList.add(createDescription());
        detailDtoList.add(createExpiration());
        detailDtoList.add(createQuantity());
        detailDtoList.add(createInvalid());
        detailDtoList.add(createQuantity());
        detailDtoList.add(createExpiration());
        detailDtoList.add(createDescription());
        Collections.shuffle(detailDtoList);

        return Collections.unmodifiableList(detailDtoList);
    }

    private DetailDto createQuantity() {
        return DetailDto.createQuantity(8, 1);
    }

    private DetailDto createDescription() {
        return DetailDto.createDescription("LA CESTERA",
                "Trenza con relleno",
                "3 sabores: Nata con almendras, nata con frutas confitadas o trufa");
    }

    private DetailDto createExpiration() {
        return DetailDto.createExpiration(2);
    }

    private DetailDto createInvalid() {
        final int invalidType = 83;
        return new DetailDto(invalidType, null,
                null, null, null, null, null);
    }
}