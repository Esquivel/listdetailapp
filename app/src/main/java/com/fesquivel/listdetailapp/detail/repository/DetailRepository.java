package com.fesquivel.listdetailapp.detail.repository;

import androidx.lifecycle.LiveData;

import com.fesquivel.listdetailapp.detail.domain.Detail;

import java.util.List;

public interface DetailRepository<T> {

    /**
     * Get information related to user.
     *
     * @param userId The user id.
     * @param saleId The sale Id.
     * @return The live data.
     */
    LiveData<T> get(final String userId, final String saleId);

    /**
     * Gets the value of state.
     *
     * @return The actual state.
     */
    LiveData<State> getState();
}