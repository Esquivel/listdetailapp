package com.fesquivel.listdetailapp.detail.repository;

import java.util.List;

import io.reactivex.Observable;

public interface DetailApi {

    /**
     * @GET("sale/detail/")
     * @Query("sale_id")
     * @Query("user_id")
     *
     * @param userId The user id.
     * @param saleId The sale id.
     * @return The list sale dto wrapped on a Observable.
     */
    Observable<List<DetailDto>> get(final String userId, final String saleId);
}
