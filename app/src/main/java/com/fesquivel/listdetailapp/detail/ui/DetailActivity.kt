package com.fesquivel.listdetailapp.detail.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.fesquivel.listdetailapp.R
import com.fesquivel.listdetailapp.detail.domain.Detail
import com.fesquivel.listdetailapp.detail.repository.DetailContainer
import com.fesquivel.listdetailapp.detail.repository.ViewState
import com.fesquivel.listdetailapp.detail.ui.view.HorizontalBannerView
import com.fesquivel.listdetailapp.home.domain.Banner
import com.fesquivel.listdetailapp.home.domain.Item
import com.squareup.picasso.Picasso

class DetailActivity : AppCompatActivity() {

    private lateinit var viewModel : DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_activity_layout)
        setToolbar()

        val container = DetailContainer()
        val innerViewModel: DetailViewModel by viewModels { ViewModelFactory(this,
            container.provideRepository(), intent.extras) }
        viewModel = innerViewModel

        val imageView = findViewById<ImageView>(R.id.image)
        val bannerView = findViewById<HorizontalBannerView>(R.id.bannerView)
        val swipeView = findViewById<SwipeRefreshLayout>(R.id.swipeRefresh)

        val adapter = DetailAdapter()
        val recyclerView = findViewById<RecyclerView>(R.id.recycler)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        viewModel.details.observe(this, Observer { list: List<Detail> ->
            adapter.setData(list)
        })

        viewModel.item.observe(this, Observer { item: Item ->
            Picasso.with(this)
                .load(item.image)
                .placeholder(R.drawable.ic_launcher_background)
                .fit()
                .centerInside()
                .into(imageView)
        })

        viewModel.banner.observe(this, Observer { banner: Banner? ->
            if (banner == null) {
                return@Observer
            }

            bannerView.visibility = View.VISIBLE
            bannerView.setTitleView(banner.title)
            bannerView.setSubTitleView(banner.subtitle)
        })

        viewModel.loadingState.observe(this, Observer { state ->
            swipeView.isRefreshing = state.getViewState() == ViewState.LOADING
        })

        swipeView.setOnRefreshListener {
            viewModel.onRefreshData()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val view = super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.detail_menu, menu)
        return view
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.refresh) {
            viewModel.onRefreshData()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setToolbar() {
        val toolbar: Toolbar = findViewById(R.id.toolbarCollapsible)
        setSupportActionBar(toolbar)
        supportActionBar?.title = null
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }
}
