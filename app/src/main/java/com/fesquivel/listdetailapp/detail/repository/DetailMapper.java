package com.fesquivel.listdetailapp.detail.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

import com.fesquivel.listdetailapp.detail.domain.DescriptionDetail;
import com.fesquivel.listdetailapp.detail.domain.Detail;
import com.fesquivel.listdetailapp.detail.domain.DetailType;
import com.fesquivel.listdetailapp.detail.domain.ExpirationDetail;
import com.fesquivel.listdetailapp.detail.domain.QuantityDetail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Mapper. Here do all validations.
 *
 * This logic must be executed on a {@link WorkerThread}.
 */
class DetailMapper {

    /**
     * Mapping logic.
     *
     * @param detailDtoList The object to map.
     * @return The result operation.
     */
    @NonNull
    @WorkerThread
    List<Detail> mapTo(@Nullable final List<DetailDto> detailDtoList) {
        final List<Detail> validDetails = validateData(detailDtoList);
        return Collections.unmodifiableList(validDetails);
    }

    /**
     * Do validations and creates the internal representation.
     * Here will be not included those DetailType that do not applies to actual models.
     *
     * @param detailDtoList The list to validate.
     * @return The
     */
    private List<Detail> validateData(@Nullable final List<DetailDto> detailDtoList) {
        final List<Detail> validatedList = new ArrayList<>();

        if (detailDtoList == null || detailDtoList.isEmpty()) {
            return validatedList;
        }

        Detail detail;

        for (final DetailDto detailDto : detailDtoList) {
            switch (detailDto.type) {
                case DetailType.DESCRIPTION:
                    detail = new DescriptionDetail(detailDto.title, detailDto.subTitle,
                            detailDto.detail);
                    break;
                case DetailType.EXPIRATION:
                    detail = new ExpirationDetail(detailDto.validDays);
                    break;
                case DetailType.QUANTITY:
                    detail = new QuantityDetail(detailDto.maxProduct, detailDto.maxQuantity);
                    break;
                default:
                    detail = null;
                    break;
            }

            if (detail != null) {
                validatedList.add(detail);
            }
        }

        return validatedList;
    }
}