package com.fesquivel.listdetailapp.detail.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

class RepositoryStateHandler {

    private final MutableLiveData<State> stateMutableLiveData = new MutableLiveData<>();

    void setLoadingState() {
        setState(() -> ViewState.LOADING);
    }

    void setReadyState() {
        setState(() -> ViewState.READY);
    }

    private void setState(final State state) {
        stateMutableLiveData.postValue(state);
    }

    public LiveData<State> getState() {
        return stateMutableLiveData;
    }
}