package com.fesquivel.listdetailapp.detail.ui.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fesquivel.listdetailapp.R;

public class ExpirationDetailViewHolder extends RecyclerView.ViewHolder {

    private final TextView expirationDateView;

    public ExpirationDetailViewHolder(@NonNull final View itemView) {
        super(itemView);

        expirationDateView = itemView.findViewById(R.id.expirationDate);
    }

    public TextView getExpirationDateView() {
        return expirationDateView;
    }
}