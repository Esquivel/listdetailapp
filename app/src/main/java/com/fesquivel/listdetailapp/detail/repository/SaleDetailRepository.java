package com.fesquivel.listdetailapp.detail.repository;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.fesquivel.listdetailapp.commons.SchedulerProvider;
import com.fesquivel.listdetailapp.detail.domain.Detail;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.disposables.Disposable;

final class SaleDetailRepository implements DetailRepository<List<Detail>> {

    private static final int SIMULATED_DELAY = 2000;

    private final DetailApi api;
    private final SchedulerProvider schedulerProvider;
    private final DetailMapper mapper;
    private final RepositoryStateHandler stateHandler;
    private final LiveData<State> stateLiveData;
    private final MutableLiveData<List<Detail>> liveData = new MutableLiveData<>();

    @Nullable
    private Disposable disposable;

    SaleDetailRepository(final DetailApi api, final SchedulerProvider schedulerProvider,
                         final DetailMapper mapper, final RepositoryStateHandler stateHandler) {
        this.api = api;
        this.schedulerProvider = schedulerProvider;
        this.mapper = mapper;
        this.stateHandler = stateHandler;

        stateLiveData = stateHandler.getState();
    }

    @Override
    public LiveData<List<Detail>> get(final String userId, final String saleId) {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }

        stateHandler.setLoadingState();

        disposable = api.get(userId, saleId)
                .map(mapper::mapTo)
                .delaySubscription(SIMULATED_DELAY, TimeUnit.MILLISECONDS)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(response -> {
                    stateHandler.setReadyState();
                    liveData.postValue(Collections.unmodifiableList(response));
                });

        return liveData;
    }

    @Override
    public LiveData<State> getState() {
        return stateLiveData;
    }
}