package com.fesquivel.listdetailapp.detail.ui

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.savedstate.SavedStateRegistryOwner
import com.fesquivel.listdetailapp.detail.domain.Detail
import com.fesquivel.listdetailapp.detail.repository.DetailRepository
import com.fesquivel.listdetailapp.home.domain.Item

class ViewModelFactory(
    owner: SavedStateRegistryOwner,
    val repository: DetailRepository<List<Detail>>,
    defaultArgs: Bundle? = null
) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    override fun <T : ViewModel?> create(key: String, modelClass: Class<T>,
        state: SavedStateHandle): T {
        return DetailViewModel(state, repository) as T
    }
}
