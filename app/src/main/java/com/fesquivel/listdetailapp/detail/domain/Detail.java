package com.fesquivel.listdetailapp.detail.domain;

import java.io.Serializable;

public interface Detail extends Serializable {

    /**
     * Returns the detail type.
     *
     * @return The type.
     */
    @DetailType
    int getType();
}
