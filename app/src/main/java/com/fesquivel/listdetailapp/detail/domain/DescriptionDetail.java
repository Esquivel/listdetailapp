package com.fesquivel.listdetailapp.detail.domain;

public class DescriptionDetail implements Detail {

    private static final long serialVersionUID = -4546353726698312802L;

    private final String title;
    private final String subtitle;
    private final String detail;

    public DescriptionDetail(final String title, final String subtitle, final String detail) {
        this.title = title;
        this.subtitle = subtitle;
        this.detail = detail;
    }

    @Override
    @DetailType
    public int getType() {
        return DetailType.DESCRIPTION;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getDetail() {
        return detail;
    }
}
