package com.fesquivel.listdetailapp.detail.repository;

import com.fesquivel.listdetailapp.detail.domain.DetailType;

/**
 * This representation simplifies the DTO. This could be differ from a productive response.
 * Created only for practical purposes.
 */
class DetailDto {

    public final Integer type;
    public final String title;
    public final String subTitle;
    public final String detail;
    public final Integer validDays;
    public final Integer maxProduct;
    public final Integer maxQuantity;

    DetailDto(final Integer type, final String title,
              final String subTitle, final String detail, final Integer validDays,
              final Integer maxProduct, final Integer maxQuantity) {
        this.type = type;
        this.title = title;
        this.subTitle = subTitle;
        this.detail = detail;
        this.validDays = validDays;
        this.maxProduct = maxProduct;
        this.maxQuantity = maxQuantity;
    }

    /**
     * Static factory method to help on the creation of test data.
     *
     * @param maxProduct
     * @param maxQuantity
     * @return
     */
    public static DetailDto createQuantity(final Integer maxProduct, final Integer maxQuantity) {
        return new DetailDto(DetailType.QUANTITY, null, null, null,
                null, maxProduct, maxQuantity);
    }

    public static DetailDto createDescription(final String title, final String subTitle,
                                              final String detail) {
        return new DetailDto(DetailType.DESCRIPTION, title, subTitle, detail, null,
                null, null);
    }

    public static DetailDto createExpiration(final Integer validDays) {
        return new DetailDto(DetailType.EXPIRATION, null,
                null, null, validDays, null, null);
    }
}