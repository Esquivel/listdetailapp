package com.fesquivel.listdetailapp.detail.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.fesquivel.listdetailapp.R;

public class HorizontalBannerView extends LinearLayout {

    private TextView titleView;
    private TextView subTitleView;

    public HorizontalBannerView(@NonNull final Context context) {
        super(context);
        init(context);
    }

    public HorizontalBannerView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public HorizontalBannerView(@NonNull final Context context, @Nullable final AttributeSet attrs,
                                final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(final Context context) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View container = inflater.inflate(R.layout.horizontal_banner_view_layout, this);

        setOrientation(HORIZONTAL);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        setBackground(ContextCompat.getDrawable(getContext(), R.color.colorPrimaryDark));
        setFocusable(true);

        titleView = container.findViewById(R.id.title);
        subTitleView = container.findViewById(R.id.subTitle);
    }

    public void setTitleView(final String title) {
        titleView.setText(title);
    }

    public void setSubTitleView(final String subTitle) {
        subTitleView.setText(subTitle);
    }
}
