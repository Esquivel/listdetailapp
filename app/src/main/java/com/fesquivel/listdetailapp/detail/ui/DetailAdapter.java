package com.fesquivel.listdetailapp.detail.ui;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fesquivel.listdetailapp.R;
import com.fesquivel.listdetailapp.detail.domain.DescriptionDetail;
import com.fesquivel.listdetailapp.detail.domain.Detail;
import com.fesquivel.listdetailapp.detail.domain.DetailType;
import com.fesquivel.listdetailapp.detail.domain.ExpirationDetail;
import com.fesquivel.listdetailapp.detail.domain.QuantityDetail;
import com.fesquivel.listdetailapp.detail.ui.viewholder.DescriptionDetailViewHolder;
import com.fesquivel.listdetailapp.detail.ui.viewholder.ExpirationDetailViewHolder;
import com.fesquivel.listdetailapp.detail.ui.viewholder.QuantityDetailViewHolder;

import java.util.ArrayList;
import java.util.List;

public class DetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<Detail> details = new ArrayList<>();

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent,
                                                      final int viewType) {
        switch (viewType) {
            case DetailType.EXPIRATION:
                return new ExpirationDetailViewHolder(inflate(parent, R.layout.expiration_detail_layout));
            case DetailType.DESCRIPTION:
                return new DescriptionDetailViewHolder(inflate(parent, R.layout.description_detail_layout));
            default:
                return new QuantityDetailViewHolder(inflate(parent, R.layout.quantity_detail_layout));
        }
    }

    private View inflate(final ViewGroup parent, @LayoutRes final int layoutResId) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutResId, parent, false);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        final Detail detail = details.get(position);

        switch (holder.getItemViewType()) {
            case DetailType.DESCRIPTION:
                createDescriptionViewHolder(holder, detail);
                break;
            case DetailType.EXPIRATION:
                createExpirationViewHolder(holder, detail);
                break;
            default:
                createQuantityViewHolder(holder, detail);
                break;
        }
    }

    @Override
    public int getItemViewType(final int position) {
        return details.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public void setData(final List<Detail> details) {
        this.details.clear();
        this.details.addAll(details);

        notifyDataSetChanged();
    }

    private void createDescriptionViewHolder(
            final RecyclerView.ViewHolder holder, final Detail detail) {
        final DescriptionDetailViewHolder descriptionHolder =
                (DescriptionDetailViewHolder) holder;
        final DescriptionDetail descriptionDetail = (DescriptionDetail) detail;
        descriptionHolder.getTitleView().setText(descriptionDetail.getTitle());
        descriptionHolder.getSubTitleView().setText(descriptionDetail.getSubtitle());
        descriptionHolder.getDetailView().setText(descriptionDetail.getDetail());
    }

    private void createExpirationViewHolder(
            final RecyclerView.ViewHolder holder, final Detail detail) {
        final Resources resources = getResources(holder);

        final ExpirationDetailViewHolder expirationHolder =
                (ExpirationDetailViewHolder) holder;
        final ExpirationDetail expirationDetail = (ExpirationDetail) detail;
        expirationHolder.getExpirationDateView().setText(
                resources.getQuantityString(R.plurals.expiration_detail_text,
                        expirationDetail.getValidDays(), expirationDetail.getValidDays()));
    }

    private void createQuantityViewHolder(final RecyclerView.ViewHolder holder,
                                          final Detail detail) {
        final Resources resources = getResources(holder);

        final QuantityDetailViewHolder quantityHolder = (QuantityDetailViewHolder) holder;
        final QuantityDetail quantityDetail = (QuantityDetail) detail;
        quantityHolder.getMaxProductView().setText(
                resources.getQuantityString(R.plurals.max_product_detail_text,
                        quantityDetail.getMaxProduct(), quantityDetail.getMaxProduct()));
        quantityHolder.getMaxValidationView().setText(
                resources.getQuantityString(R.plurals.max_quantity_detail_text,
                        quantityDetail.getMaxQuantity(), quantityDetail.getMaxQuantity())
        );
    }

    @NonNull
    private Resources getResources(final RecyclerView.ViewHolder holder) {
        return holder.itemView.getContext().getResources();
    }
}
