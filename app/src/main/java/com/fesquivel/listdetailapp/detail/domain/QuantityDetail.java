package com.fesquivel.listdetailapp.detail.domain;

public class QuantityDetail implements Detail {

    private static final long serialVersionUID = 8991095116794335573L;

    private final Integer maxProduct;
    private final Integer maxQuantity;

    public QuantityDetail(final Integer maxProduct, final Integer maxQuantity) {
        this.maxProduct = maxProduct;
        this.maxQuantity = maxQuantity;
    }

    @Override
    public int getType() {
        return DetailType.QUANTITY;
    }

    public Integer getMaxProduct() {
        return maxProduct;
    }

    public Integer getMaxQuantity() {
        return maxQuantity;
    }
}
