package com.fesquivel.listdetailapp.detail.ui.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fesquivel.listdetailapp.R;

public class QuantityDetailViewHolder extends RecyclerView.ViewHolder {

    private final TextView maxProductView;
    private final TextView maxValidationView;

    public QuantityDetailViewHolder(@NonNull final View itemView) {
        super(itemView);

        maxProductView = itemView.findViewById(R.id.maxProduct);
        maxValidationView = itemView.findViewById(R.id.maxValidation);
    }

    public TextView getMaxProductView() {
        return maxProductView;
    }

    public TextView getMaxValidationView() {
        return maxValidationView;
    }
}