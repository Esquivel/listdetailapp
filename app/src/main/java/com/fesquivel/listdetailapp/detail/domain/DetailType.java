package com.fesquivel.listdetailapp.detail.domain;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@IntDef({
        DetailType.DESCRIPTION,
        DetailType.EXPIRATION,
        DetailType.QUANTITY
})
public @interface DetailType {
    int DESCRIPTION = 0;
    int EXPIRATION = 1;
    int QUANTITY = 2;
}