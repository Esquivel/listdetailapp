package com.fesquivel.listdetailapp.detail.ui.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fesquivel.listdetailapp.R;

public class DescriptionDetailViewHolder extends RecyclerView.ViewHolder {

    private final TextView titleView;
    private final TextView subTitleView;
    private final TextView detailView;

    public DescriptionDetailViewHolder(@NonNull final View itemView) {
        super(itemView);

        titleView = itemView.findViewById(R.id.title);
        subTitleView = itemView.findViewById(R.id.subtitle);
        detailView = itemView.findViewById(R.id.detail);
    }

    public TextView getTitleView() {
        return titleView;
    }

    public TextView getSubTitleView() {
        return subTitleView;
    }

    public TextView getDetailView() {
        return detailView;
    }
}