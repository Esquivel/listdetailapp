# ListDetail App

Welcome to this sample project. You will find an very simple application with two screens:

- List Screen:
    Here find a collection of Items. This items can handle events like Click on it or on their Buttons. 
    Also will find a swipe to refresh, that simulates an API call to refresh your data.
    The first time you are on this screen the app request a simulated login action.
    Perhaps the most important aspect in this layer is the treatment of the Android lifecycle, reconstruction and the related Presenter State.

- Detail Screen:
    Here find the detail of the Item selected. Here, again, can refresh your data. The refresh behaviour is shuffle the internal data mocked to simulate a data change.
    The detail screen has a heterogeneous inside RecyclerView. This architecture is flexible, simple and retrocompatible. Those types the View not know how draw will be filtered on the Model layer (Repository) by a Mapper. 

## UX

This app is inspired by the [Material Design Guidelines](https://material.io/design).
It was designed supporting [accessibility](https://developer.android.com/guide/topics/ui/accessibility) best practices  

## Architecture
Here will find examples to how implement two different Architecture patterns:

- [Model View Presenter](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter) 
- [Model View ViewModel](https://developer.android.com/jetpack/guide#recommended-app-arch) This is a special implementation developed by Google included on [JetPack Libraries](https://developer.android.com/jetpack)

## Languages
This app was developed mixing Java and Kotlin.